let assert = require('assert');
let rules = require('../rules.js');

const DEAD = 0;
const ALIVE = 1;
const conway = rules.conway;
const panoply = rules.panoply;

// Rule 1 - Live cell with less  than 2 neighbours dies.
assert.equal(conway([[ DEAD, DEAD, DEAD, DEAD, DEAD, DEAD ]],ALIVE), DEAD);
assert.equal(conway([[ ALIVE, DEAD, DEAD, DEAD, DEAD, DEAD ]],ALIVE), DEAD);
// Rule 2 - Live cell with 2 or 3 neighbours stays alive.
assert.equal(conway([[ ALIVE, DEAD, DEAD, ALIVE, ALIVE, DEAD ]], ALIVE), ALIVE);
assert.equal(conway([[ ALIVE, DEAD, DEAD, ALIVE, DEAD, DEAD ]], ALIVE), ALIVE);
// Rule 3 - Live cell with more than 3 neighbours dies.
assert.equal(conway([[ ALIVE, DEAD, ALIVE, ALIVE, ALIVE, DEAD ]], ALIVE), DEAD);
// Rule 4 - Dead cell with exactly 3 neighbours becomes alive
assert.equal(conway([[ ALIVE, ALIVE, ALIVE, DEAD, DEAD, DEAD ]], DEAD), ALIVE);
assert.equal(conway([[ ALIVE, ALIVE, ALIVE, ALIVE, DEAD, DEAD ]], DEAD), DEAD);
// Rule 5 - Dead cell with exactly one neighbour lives
assert.equal(panoply([[ ALIVE, DEAD, DEAD, DEAD, DEAD, DEAD ]], DEAD), ALIVE);
assert.equal(panoply([[ ALIVE, ALIVE, DEAD, DEAD, DEAD, DEAD ]], DEAD), DEAD);
// Rule 6 - live cell with no living horizontal neighbours
assert.equal(panoply({
    horizontal: [ DEAD, DEAD, DEAD ],
    vertical: [ DEAD, DEAD, DEAD ],
}, ALIVE), DEAD);

assert.equal(panoply({
    horizontal: [ ALIVE, DEAD, DEAD ],
    vertical: [ DEAD, DEAD, DEAD ],
}, ALIVE), ALIVE);
