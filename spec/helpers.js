let assert = require('assert');
let helpers = require('../helpers.js');

assert.deepEqual(helpers.unFlatten(2,2,[1,0,0,1]), [[1,0],[0,1]]);
assert.deepEqual(helpers.unFlatten(2,3,[1,0,0,1,1,1]), [[1,0],[0,1],[1,1]]);
assert.deepEqual(helpers.unFlatten(3,2,[1,0,0,1,1,1]), [[1,0,0],[1,1,1]]);

assert.deepEqual(helpers.flatten([ [1,2,3] , [4,5,6] ]), [1,2,3,4,5,6]);
assert.deepEqual(helpers.flatten([ 1,2,3,4,5,6 ]), [1,2,3,4,5,6]);
