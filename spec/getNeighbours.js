let assert = require('assert');
let getNeighbours = require('../getNeighbours.js');

//Single
assert.deepEqual(
    getNeighbours([
    [ 1 ],
],0,0), (r => {
    r.horizontal = [ 0 ];
    r.vertical = [ 0 ];
    return r;
})([
    [ 0 ]
]));

// Single Row
assert.deepEqual(
    getNeighbours([
    [ 1, 0, 1 ],
],0,1), (r => {
    r.horizontal = [ 1, 0, 1 ];
    r.vertical = [ 0 ];
    return r;
})([
    [ 1, 0, 1 ]
]));

// Single Column
assert.deepEqual(
    getNeighbours([
    [ 1 ],
    [ 0 ],
    [ 1 ],
],0,0), (r => {
    r.horizontal = [ 0 ];
    r.vertical = [ 0, 0 ];
    return r;
})([
    [ 0 ],
    [ 0 ]
]));

// Top left
assert.deepEqual(
    getNeighbours([
        [0, 1, 0],
        [0, 1, 0],
        [0, 0, 0],
    ], 0, 0), (r => {
        r.horizontal = [ 0, 1 ];
        r.vertical = [ 0, 0 ];
        return r;
    })([
        [0, 1],
        [0, 1]
    ]));

// Bottom Right
assert.deepEqual(
    getNeighbours([
        [0, 1, 0],
        [0, 1, 0],
        [0, 0, 0],
    ], 2, 2), (r => {
        r.horizontal = [ 0, 0 ];
        r.vertical = [ 0, 0 ];
        return r;
    })([
        [1, 0],
        [0, 0]
    ]));


// Middle
assert.deepEqual(
    getNeighbours([
        [0, 1, 0],
        [0, 1, 0],
        [0, 0, 0],
    ], 1, 1), (r => {
        r.horizontal = [ 0, 0, 0 ];
        r.vertical = [ 1, 0, 0 ];
        return r;
    })([
        [0, 1, 0],
        [0, 0, 0],
        [0, 0, 0],
    ]));

// Left Middle
assert.deepEqual(
    getNeighbours([
        [1, 1, 1],
        [1, 1, 1],
        [0, 0, 0],
    ], 1, 0), (r => {
        r.horizontal = [ 0, 1 ];
        r.vertical = [ 1, 0, 0 ];
        return r;
    })([
        [1, 1],
        [0, 1],
        [0, 0],
    ]));

// Left Bottom
assert.deepEqual(
    getNeighbours([
        [1, 1, 1],
        [1, 1, 1],
        [0, 0, 0],
    ], 2, 0), (r => {
        r.horizontal = [ 0, 0 ];
        r.vertical = [ 1, 0 ];
        return r;
    })([
        [1, 1],
        [0, 0],
    ]));

// Right Middle
assert.deepEqual(
    getNeighbours([
        [1, 1, 1],
        [1, 1, 1],
        [0, 0, 0],
    ], 1, 2), (r => {
        r.horizontal = [ 1, 0 ];
        r.vertical = [ 1, 0, 0 ];
        return r;
    })([
        [1, 1],
        [1, 0],
        [0, 0],
    ]));


// Bottom Middle
assert.deepEqual(
    getNeighbours([
        [1, 1, 0],
        [1, 1, 0],
        [0, 0, 1],
    ], 2, 1), (r => {
        r.horizontal = [ 0, 0, 1 ];
        r.vertical = [ 1, 0 ];
        return r;
    })([
        [1, 1, 0],
        [0, 0, 1],
    ]));
