let flatten = require('./helpers.js').flatten;

const DEAD = 0;
const ALIVE = 1;
// neighbours are including the cell so we count the total in the block and then subtract the cell.
const livingNeighbours = (neighbours) => flatten(neighbours).filter(n => n == ALIVE).length;

const conway = (neighbours, isAlive) => {
    let s = livingNeighbours(neighbours);
    if (isAlive)
    {
        if (s < 2)
            return DEAD;
        if (s > 3)
            return DEAD;
        else
            return ALIVE;
    }
    if (s == 3)
        return ALIVE;

    return DEAD;
}

const panoply = (neighbours, isAlive) => {
    if (isAlive) {
        if (livingNeighbours(neighbours.vertical.concat(neighbours.horizontal)))
            return ALIVE;
        return DEAD;
    }
    let s = livingNeighbours(neighbours);
    if (s == 1)
        return ALIVE;

    return DEAD;
}
module.exports = {
    conway: conway,
    panoply: panoply,
};
