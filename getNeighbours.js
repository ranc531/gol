let assert = require ('assert');

// Get the surrounding neighbours of a cell (including self). returns an array of arrays (rows, columns) with the vertical and horizontal references attached.

const getNeighbours = (board, i, j) => {

    function getRow(i, from, count) { return board[i].slice(Math.max(from,0), Math.min(from+count, board[0].length)) }
    function getCol(r, j) { return r.map(row => row[j]); }

    count = 3; // 3x3 neighbours.

    let retVal = [];

    // if this is the first/last row skip the first/last row.
    for (k = Math.max(i-1,0); k <= Math.min(i-1+count-1,board.length-1); ++k)
        retVal.push(getRow(k,j-1,count));

    // zero the middle cell so we dont count it when we count living neighbours.
    retVal[Math.min(i,1)][Math.min(j,1)] = 0;

    // if this is the first row the first row of neighbours is the horizontal.
    retVal.horizontal = retVal[Math.min(i,1)];

    // if this is the first column the first column of neighbours is the vertical.
    retVal.vertical = getCol(retVal, Math.min(j,1));

    return retVal;
}

module.exports = getNeighbours;
