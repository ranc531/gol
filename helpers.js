let assert = require('assert');

const unFlatten = (w, h, flatArray) =>  {
    assert.equal(flatArray.length, w*h, 'Array size does not match width and height');
    let ret = [];
    for(i=0; i < h; ++i) {
        ret.push([]);
        for (j=0; j < w; ++j) {
            ret[i].push(flatArray[i*w+j]);
        }
    }
    return ret;
};

const flatten = function (array) { return Array.isArray(array[0]) ? array.reduce((l,r) => { r.forEach(i => l.push(i)); return l; }, []) : array; }

module.exports = {
    unFlatten: unFlatten,
    flatten: flatten
}
