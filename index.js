// Conways Game of Life.

let assert = require ('assert');
let helpers = require('./helpers.js');
let rules = require('./rules.js');
let getNeighbours = require('./getNeighbours.js');

// defines the transition of a generation
const generation = (s) => originalBoard =>
    originalBoard.map((row, i) => row.map((col,j) => s(getNeighbours(originalBoard, i,j), originalBoard[i][j])))

function* run(infectAfter, maxGenerations, board) {

    infectAfter = Math.min(infectAfter, maxGenerations-1);

    let normalGeneration = generation(rules.conway);
    let infectionGeneration = generation(rules.panoply);

    if (maxGenerations) yield board;
    for (i = 0; i < infectAfter; ++i)
    {
        board = normalGeneration(board);
        yield board;
    }
    for (i = 1; i < maxGenerations-infectAfter; ++i)
    {
        board = infectionGeneration(board);
        yield board;
    }
}

// PDF Example
assert.deepEqual(generation(rules.conway)([
    [ 0, 0, 0 ],
    [ 1, 0, 0 ],
    [ 1, 0, 1 ],
]), [
    [ 0, 0, 0 ],
    [ 0, 1, 0 ],
    [ 0, 1, 0 ],
]);

const cli = (width, height, infectAfter, maxGenerations, seed) => run(
              parseInt(infectAfter),
              parseInt(maxGenerations),
              helpers.unFlatten(parseInt(width), parseInt(height), seed.split(" ").map(s => parseInt(s)))
          );

for (let generation of cli.apply(null,process.argv.slice(2)))
{
    console.log(generation.map(row => row.join(" ")).join(" "));
}
